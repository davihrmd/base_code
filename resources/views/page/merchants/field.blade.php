<div class="row">
    <div class="col-md-6 ">
        <div class="form-body form-group form-md-line-input">
            <input class="form-control" type="text" name="username" value="{{ @$data->user->username }}">
            <label>Username</label>
        </div>
        <div class="form-body form-group form-md-line-input">
            <input class="form-control" type="password" name="password" value="">
            <label>Password</label>
        </div>
        <div class="form-body form-group form-md-line-input">
            <input class="form-control" type="text" name="name" value="{{ @$data->name }}">
            <label>Nama</label>
        </div>
        <div class="form-body form-group form-md-line-input">
            <input class="form-control" type="text" name="address" value="{{ @$data->address }}">
            <label>Alamat</label>
        </div>
        <div class="form-body form-group form-md-line-input">
            <input class="form-control" type="text" name="contact_number" value="{{ @$data->contact_number }}">
            <label>Nomor Kontak</label>
        </div>

    </div>
</div>
<button class="btn green" type="submit">Save</button>
<button class="btn red"> Cancel </button>
