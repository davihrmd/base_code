 <tr>
    <td style="width: 30%">
        <div class="form-group form-md-line-input has-success">
            {!! Template::selectbox(['' => ' - Choose Product - '] + $listProduct->toArray(), @$detail->product_id, "product_id[]", ["class" => "form-control product_id select2"]) !!}
        </div>
    </td>
    <td style="width: 10%">
        <div class="form-group form-md-line-input has-success">
            <input type="text" name="qty[]" class="form-control qty" value="{{ @$detail->qty }}">
        </div>
    </td>
    <td>
        <div class="form-group form-md-line-input has-success">
            <input type="text" name="price[]" class="form-control price" value="{{ @$detail->price }}">
        </div>
    </td>
    <td>
        <div class="form-group form-md-line-input has-success">
            <input type="text" name="subtotal[]" class="form-control subtotal" value="{{ @$detail->subtotal }}">
        </div>
    </td>
    <td><a class="hapus" title="Delete Record" id=""><i class="fa fa-trash-o"></i></a></td>
</tr>