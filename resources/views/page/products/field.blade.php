<div class="row">
    <div class="col-md-6 ">
        <div class="form-body form-group form-md-line-input">
            {!! Template::selectbox($listProductCategory,@$data->product_category_id,"product_category_id",[ 'class' => 'form-control' ]) !!}
            <label>Produk Kategori</label>
        </div>
        <div class="form-body form-group form-md-line-input">
            {!! Template::selectbox($listUnit,@$data->unit_id,"unit_id",[ 'class' => 'form-control' ]) !!}
            <label>Satuan</label>
        </div>
        <div class="form-body form-group form-md-line-input">
            <input class="form-control" type="text" name="name" value="{{ @$data->name }}">
            <label>Nama</label>
        </div>
        <div class="form-body form-group form-md-line-input">
            <input class="form-control" type="text" name="description" value="{{ @$data->description }}">
            <label>Keterangan</label>
        </div>
        <div class="form-body form-group form-md-line-input">
            <input class="form-control" type="text" name="price" value="{{ @$data->price }}">
            <label>Harga</label>
        </div>
        <div class="form-body form-group form-md-line-input">
            <input class="form-control" type="text" name="stock" value="{{ @$data->stock }}">
            <label>Stok</label>
        </div>
    </div>
</div>
<button class="btn green" type="submit">Save</button>
<button class="btn red"> Cancel </button>
