<html>
<head>

</head>
<body>
    <table cellpadding="10" cellspacing="1" border="1">
        <tr>
            <th>Nama Barang</th>
            <th>Qty</th>
            <th>Price</th>
            <th>Subtotal</th>
        </tr>
        @foreach($datas as $data)
        <tr>
            <td>{{ $data->product->name }}</td>
            <td>{{ $data->qty }}</td>
            <td>{{ $data->price }}</td>
            <td>{{ $data->price * $data->qty }}</td>
        </tr>
        @endforeach
    </table>
</body>
</html>