<div class="panel panel-default">
    <div class="panel-heading">Data Transaksi</div>
    <div class="panel-body">

    <div class="row">
        <div class="col-md-3">
            <div class="form-group form-md-line-input has-success">
                {!! Template::selectbox(['' => ' - Choose Customer - '] + $listCustomer->toArray(), @$data->customer_id, "customer_id", ["class" => "form-control"]) !!}
                <label for="form_control_1">Customer</label>
            </div>
            <div class="form-group form-md-line-input has-success">
                {!! Template::selectbox(['' => ' - Choose Seller - '] + $listSeller->toArray(), @$data->seller_id, "seller_id", ["class" => "form-control"]) !!}
                <label for="form_control_1">Penjual</label>
            </div>
            <div class="form-group form-md-line-input has-success">
                <input type="text" name="number_invoice" class="form-control" value="{{ (!empty($data->number_invoice) ? $data->number_invoice : date('YmdHis')) }}">
                <label for="form_control_1">Number Invoice</label>
            </div>
            <div class="form-group form-md-line-input has-success">
                <input type="text" name="order_date" class="form-control datepicker" value="{{ @$data->order_date }}">
                <label for="form_control_1">Order Date</label>
            </div>
            <div class="form-group form-md-line-input has-success">
                <input type="text" name="delivery_date" class="form-control datepicker" value="{{ @$data->delivery_date }}">
                <label for="form_control_1">Delivery Date</label>
            </div>
            <div class="form-group form-md-line-input has-success">
                <textarea class="form-control" name="order_address" rows="2" value="">{{ @$data->order_address }}</textarea>
                <label for="form_control_1">Order Address</label>
            </div>
        </div>
        <div class="col-md-9">

            <div class="panel panel-default">
                <div class="panel-heading">Detail Transaksi</div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12">
                            <a class="btn btn-success" id="add_row"><i class="fa fa-plus"></i> Add</a><br><br>
                            <table class="table table-hover table-light">
                                <thead>
                                <tr>
                                    <th>Product</th>
                                    <th>Qty</th>
                                    <th>Price</th>
                                    <th>Subtotal</th>
                                    <th>Delete</th>
                                </tr>
                                </thead>
                                <tbody id="detail">
                                @if(!empty($data->order_details))
                                    @foreach($data->order_details as $detail)
                                    @php
                                    $subtotal = @$detail->qty * @$detail->price;
                                    @$total += $subtotal;
                                    @endphp
                                    <tr>
                                        <td style="width: 30%">
                                            <div class="form-group form-md-line-input has-success">
                                                {!! Template::selectbox(['' => ' - Choose Product - '] + $listProduct->toArray(), @$detail->product_id, "product_id[]", ["class" => "form-control product_id"]) !!}
                                            </div>
                                        </td>
                                        <td style="width: 10%">
                                            <div class="form-group form-md-line-input has-success">
                                                <input type="text" name="qty[]" class="form-control qty" value="{{ @$detail->qty }}">
                                            </div>
                                        </td>
                                        <td>
                                            <div class="form-group form-md-line-input has-success">
                                                <input type="text" name="price[]" class="form-control price" value="{{ @$detail->price }}">
                                            </div>
                                        </td>
                                        <td>
                                            <div class="form-group form-md-line-input has-success">
                                                <input type="text" name="subtotal[]" class="form-control subtotal" value="{{ @$subtotal }}">
                                            </div>
                                        </td>
                                        <td><a class="hapus" title="Delete Record" id=""><i class="fa fa-trash-o"></i></a></td>
                                    </tr>

                                    @endforeach
                                @endif
                                </tbody>
                                <tbody>
                                    <tr>
                                        <td colspan="3" align="right">Total:</td>
                                        <td><input type="text" name="total" class="form-control total" value="{{ @$total }}"></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<button class="btn green" type="submit">Save</button>
<button class="btn red"> Cancel </button>

@section('js')
<script>
$(document).ready(function() {

    $("body").on('change','.product_id', function(){
        var index       = $(this).index(".product_id");
        var produk_id     = $(this).val();
        $( ".price" ).eq(index).val(0);
        if(produk_id > 0)
        {
            var url = "{{ url('api/products') }}"+"/"+produk_id;
            $.get( url, function( data ) {
                console.log(data.data.price);
                if(data.data.price > 0)
                {
                    $(".price").eq(index).val(data.data.price);
                    
                }
            }, "json" );
        }
        subtotal(index);
    });

    $("body").on('click','.hapus', function(){
        $('.hapus').eq($(this).index()).parent().parent().remove();
        total();
    });

    $("body").on('keyup','.qty', function(){
        var index       = $(this).index(".qty");
        subtotal(index);
    });

    function subtotal(index){
        var qty     = $(".qty").eq(index).val();
        var price   = $(".price").eq(index).val();
        $(".subtotal").eq(index).val(qty * price);
        total();
    }

    function total(){
        var sum = 0;
        $('.subtotal').each(function(){
            sum += parseFloat($(this).val());  // Or this.innerHTML, this.innerText
        });
        $('.total').val(sum);
    }

    $("#add_row").click(function(){
        var url = "{{ url('orders/detail') }}";
        $.get( url, function( data ) {
            $("#detail").append( data );
            $('select').select2();
        });
        
    })
});
</script>
@endsection
