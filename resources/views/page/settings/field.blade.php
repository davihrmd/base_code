<div class="row">
    <div class="col-md-6 ">
        <div class="form-body form-group form-md-line-input">
            <input class="form-control" type="text" name="key" value="{{ @$data->key }}">
            <label>key</label>
        </div>
        <div class="form-body form-group form-md-line-input">
            <input class="form-control" type="text" name="value" value="{{ @$data->value }}">
            <label>value</label>
        </div>
    </div>
</div>
<button class="btn green" type="submit">Save</button>
<button class="btn red"> Cancel </button>
