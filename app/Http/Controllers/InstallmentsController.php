<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\InstallmentRequest;

use App\Models\Installment;

use Yajra\DataTables\Facades\DataTables;
use DB,General,View,JsValidator,Hash,Alert;

class InstallmentsController extends Controller
{
    public $view;
    public $main_model;

    public function __construct(Installment $main_model){
        $this->title        = 'Installment';
        $this->view         = 'installments';
        $this->main_model   = $main_model;
        $this->validate     = 'InstallmentRequest';

        View::share('view', $this->view);
        View::share('title', $this->title);
    }

    public function store(InstallmentRequest $request)
    {
        $input = $request->all();

        DB::beginTransaction();
        try {
            $data = $this->main_model->create($input);
            DB::commit();
            toast()->success('Data berhasil input', $this->title);
        } catch(\Exception $e) {
            toast()->error('Terjadi Kesalahan ' . $e->getMessage(), $this->title);
            DB::rollback();
        }
        return redirect()->back();
    }
}
