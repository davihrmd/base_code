<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\PurchaseRequest;

use App\Models\Purchase;
use App\Models\PurchaseDetail;
use App\Models\Supplier;
use App\Models\Product;

use App\Traits\ImagesTrait;
use Yajra\DataTables\Facades\DataTables;
use DB,General,View,JsValidator,Hash,Alert;

class PurchasesController extends Controller
{
    use ImagesTrait;

    public $view;
    public $main_model;

    public function __construct(Purchase $main_model){
        $this->title        = 'Pembelian';
        $this->view         = 'purchases';
        $this->main_model   = $main_model;
        $this->validate     = 'PurchaseRequest';

        $listSupplier       = Supplier::orderBy('name', 'asc')->pluck('name', 'id');
        $listProduct        = Product::orderBy('name', 'asc')->pluck('name', 'id');

        View::share('view', $this->view);
        View::share('title', $this->title);

        View::share('listSupplier', $listSupplier);
        View::share('listProduct', $listProduct);
    }

    public function index(Request $request)
    {
        $columns = ['supplier.name', 'number_invoice', 'purchase_date', 'action'];
        if($request->ajax())
        {
            $datas = $this->main_model->with(['supplier'])->get();
            return Datatables::of($datas)
                ->addColumn('action',function($data){
                        return view('page.'.$this->view.'.action',compact('data'));
                    })
                ->escapeColumns(['actions'])
                ->make(true);
        }
        return view('page.'.$this->view.'.index')
            ->with(compact('datas','columns'));
    }

    public function report_pembelian(Request $request)
    {
        $columns = ['product.name', 'purchase.purchase_date', 'price'];
        if($request->ajax())
        {
            $datas = PurchaseDetail::with(['purchase','product'])->get();
            return Datatables::of($datas)
                ->make(true);
        }
        return view('page.'.$this->view.'.report_pembelian')
            ->with(compact('datas','columns'));
    }

    public function create()
    {
        $validator = JsValidator::formRequest('App\Http\Requests\\'.$this->validate);
        return view('page.'.$this->view.'.create')->with(compact('validator'));
    }

    public function store(PurchaseRequest $request)
    {
        $input = $request->all();
        
        DB::beginTransaction();
        try {
            $input['image'] = General::setImage($request->file('image'),$this->view);            
            $data = $this->main_model->create($input);
            foreach ($input['product_id'] as $k => $v) {
                $input_detail['purchase_id']    = $data->id;
                $input_detail['product_id']     = $input['product_id'][$k];
                $input_detail['qty']            = $input['qty'][$k];
                $input_detail['price']          = $input['price'][$k];
                PurchaseDetail::create($input_detail);
            }
            DB::commit();
            toast()->success('Data berhasil input', $this->title);
            return redirect()->route($this->view.'.index');
        } catch(\Exception $e) {
            toast()->error('Terjadi Kesalahan ' . $e->getMessage(), $this->title);                        
            DB::rollback();
        }
        return redirect()->back();
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $data = $this->main_model->findOrFail($id);
        $validator = JsValidator::formRequest('App\Http\Requests\\'.$this->validate);
        return view('page.'.$this->view.'.edit')->with(compact('validator','data'));
    }

    public function update(PurchaseRequest $request, $id)
    {
        $input = $request->all();
        $data = $this->main_model->findOrFail($id);
        DB::beginTransaction();
        try{
            $input['image'] = General::setImage($request->file('image'),$this->view);            
            $data->fill($input)->save();
            foreach($data->purchase_details as $detail){
                $product    = Product::findOrFail($detail->product_id);

                $stock      = (int)$product->stock - (int)$detail->qty;
                $update_stock['stock'] = 0;
                $product->fill($update_stock)->save();
            }
            PurchaseDetail::wherePurchaseId($data->id)->delete();
            foreach ($input['product_id'] as $k => $v) {
                $input_detail['purchase_id']       = $data->id;
                $input_detail['product_id']     = $input['product_id'][$k];
                $input_detail['qty']            = $input['qty'][$k];
                $input_detail['price']          = $input['price'][$k];
                PurchaseDetail::create($input_detail);
            }
            toast()->success('Data berhasil input', $this->title);
            DB::commit();
            return redirect()->route($this->view.'.index');
        }catch(\Exception $e) {
            toast()->error('Terjadi Kesalahan ' . $e->getMessage(), $this->title);                        
            DB::rollback();
        }
        return redirect()->back();
    }

    public function destroy($id)
    {
        $data = $this->main_model->findOrFail($id);
        DB::beginTransaction();
        try{
            $data->delete();
            DB::commit();
            toast()->success('Data berhasil input', $this->title);
            return redirect()->route($this->view.'.index');
        }catch(\Exception $e) {
            toast()->error('Terjadi Kesalahan ' . $e->getMessage(), $this->title);                        
            DB::rollback();
        }
        return redirect()->back();
    }

    public function detail()
    {
        return view('page.'.$this->view.'.detail');
    }

    public function report($month=null,$year=null){
        $datas = new PurchaseDetail;
        if(!empty($month))
            $datas = $datas->whereMonth('created_at',$month);
        if(!empty($year))
            $datas = $datas->whereYear('created_at',$year);
        $datas = $datas->get();
        return view('page.'.$this->view.'.report')->with(compact('datas'));

    }

}
