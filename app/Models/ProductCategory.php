<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductCategory extends Model
{
    protected $fillable = ['parent_id','name'];

    public function parent()
    {
        return $this->belongsTo('App\Models\ProductCategory','parent_id','id');
    }
}
