<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DailyOrder extends Model
{
    
    protected $fillable = [
        'customer_id',
        'seller_id',
        'recomendation_date',
        'order_address'
    ];

    public function customer()
    {
        return $this->belongsTo('App\Models\Customer', 'customer_id');
    }

    public function seller()
    {
        return $this->belongsTo('App\Models\Seller', 'seller_id');
    }

    public function daily_order_details()
    {
        return $this->hasMany('App\Models\DailyOrderDetail' , 'order_id');
    }

    public function installments()
    {
        return $this->hasMany('App\Models\Installment' , 'order_id');
    }

}
