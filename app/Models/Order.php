<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{

    protected $fillable = [
        'customer_id',
        'seller_id',
        'number_invoice',
        'delivery_date',
        'order_date',
        'order_address'
    ];

    public function customer()
    {
        return $this->belongsTo('App\Models\Customer', 'customer_id');
    }

    public function seller()
    {
        return $this->belongsTo('App\Models\Seller', 'seller_id');
    }

    public function order_details()
    {
        return $this->hasMany('App\Models\OrderDetail' , 'order_id');
    }

    public function installments()
    {
        return $this->hasMany('App\Models\Installment' , 'order_id');
    }

}
