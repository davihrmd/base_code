<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = [
        'product_category_id', 
        'unit_id',
        'name', 
        'description',
        'price', 
        'stock'
    ];

    public function product_category()
    {
        return $this->belongsTo('App\Models\ProductCategory');
    }

    public function unit()
    {
        return $this->belongsTo('App\Models\Unit');
    }
}
